$(document).ready(function(){


///form validation start
	$("#nameError").hide();
	$("#password_error").hide();
	$("#rePassword_error").hide();
	$("#email_error").hide();

	$('#name').focusout(function(){

		check_name();

	});

	$('#password').focusout(function(){

		check_password();

	});

	$('#rePassword').focusout(function(){

		match_password();

	});

	$('#email').focusout(function(){

		email_check();

	});


	function check_name(){
		var nameLength = $('#name').val().length;
		if(nameLength < 5  || nameLength > 20){
			$('#nameError').html("name 5 to 20");
			$('#nameError').show();
			
		}else{
			$('#nameError').hide();
		}

	}

	function check_password(){
		var passwordLength = $('#password').val().length;
		if(passwordLength < 8){
			$('#password_error').html("password should be minimum 8 character");
			$('#password_error').show();
			
		}else{
			$('#password_error').hide();
		}

	}

	function match_password(){
		var passwordLength = $('#password').val();
		var re_passwordLength = $('#rePassword').val();
		if(passwordLength != re_passwordLength){
			$('#rePassword_error').html("password did not match");
			$('#rePassword_error').show();
			
		}else{
			$('#rePassword_error').hide();
		}

	}

	function email_check(){

		var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
		if(pattern.test($('#email').val())){
			$('#email_error').hide();
		}else{
			$('#email_error').html("invalid email address");
			$('#email_error').show();
		}
	}
	///form validation end

		
//dialoge show start
$('#dialougeBox').dialog();
//dialoge show end

//tooltip show start
$(document).tooltip();

$('#label').tooltip({
	content: "document"
});
$('#name').tooltip({
	content:tollTip(),
	track:true,
	show:{delay:100,duration:500,effect:'slideDown'},
	hide:{delay:100,duration:500,effect:'slideUp'}
});

function tollTip(){
	return "title change";
}
//tooltip show end

//menu show start
$('#menu').menu();
//menu show end

//slider value increment/decrement show start
$("#slider").slider({
	values:100,
	min:0,
	max:500,
	step:50,
	slide:function(event, ui){
		$('#amount').val("TK  " + ui.value);
	}

});

$('#amount').val("TK " + $('#slider').slider("value"));
//slider value increment/decrement show end

//range slider show start
	var output = $('#amountTk');
	var showSlider = $('#showSlider');

	showSlider.slider({
		range:true,
		min:10,
		max:100,
		values:[10,20],
		slide:function(event, ui){
			output.html(ui.values[0]+ '-'+ ui.values[1] + ' Taka');
			$('#minTk').val(ui.values[0]);
			$('#maxTk').val(ui.values[1]);
		}
		
	});

	output.html(showSlider.slider('values',0) + '-' + showSlider.slider('values',1) + ' Taka');
	$('#minTk').val(showSlider.slider('values',0));
	$('#maxTk').val(showSlider.slider('values',1));

// range slider show end


//image dimention control with slider start

 

 $('#heightSlider,#widthSlider').slider({
 		min:100,
 		max:500,
 		slide:controlImg
 	});
 $('#opacitySlider').slider({
 		min:0,
 		max:100,
 		value:100,
 		slide:controlImg
 	});


 	function controlImg(){

 		var heightValue = $('#heightSlider').slider('value');
 		var widthValue  = $('#widthSlider').slider('value');
 		var opacity  = $('#opacitySlider').slider('value');

 		$('#image').css({
 			height:heightValue,
 			width:widthValue,
 			opacity:opacity/100
 		});

 		$('#showValue').html("Height :" +heightValue+ " pixels </br>"+
 							"Width :" +widthValue+ " pixels </br>"+
 							"Opacity :" +opacity/100);
 	}

 		
//image dimention control with slider end

//dragable widget start
	$('#innerDiv').draggable({
		/*axis:'y'
		containment:'parent'
		containment:'#outerdiv',
		cursor:'move',
		opacity:0.5,
		revert:true*/

	});

$('.div').draggable({
	snap:'#outerdiv',
	cancel:'#outerdiv',
	snapTolerance:50
	
});
//dragable widget end 

//dragable widget priority start
$('.innderDivClass').draggable(/*stack:'.innderDivClass'*/);
$('.innderDivClass').mousedown(function(){
	var maxZindex = 0;
	$(this).siblings('.innderDivClass').each(function(){

		var currentZindex = Number($(this).css('z-index'));
		maxZindex = currentZindex > maxZindex ? currentZindex : maxZindex;
	});
	$(this).css('z-index', maxZindex+1);
});
//dragable widget priority start

//Draggable and Droppable widget show start

$('#list li').draggable({
	helper:function(){
		return "<u><b>"+$(this).text()+"</b></u>";
	},
	revert:'invalid',
	cursor:'move'
});

$('#phpframework').droppable({
	accept:'li[data-value ="php"]',
	/*hoverClass:'widget',
	activeClass:'widget'
	*/
	activate:function(event, ui){
		$(this).addClass('widget');
	},
	deactivate:function(event, ui){
		$(this).removeClass('widget');
	},
	
	drop:function(event, ui){
		$("#php").append(ui.draggable);
	}
});

$('#javaframework').droppable({
	accept:'li[data-value ="java"]',
	/*hoverClass:'widget',
	activeClass:'widget'
	*/
	activate:function(event, ui){
		$(this).addClass('widget');
	},
	deactivate:function(event, ui){
		$(this).removeClass('widget');
	},
	drop:function(event, ui){
		$("#java").append(ui.draggable);
	}
});

//Draggable and Droppable widget show start

//resizeable wedget option start
$('#box1').resizable({
	/*alsoResize:"#box2"
	containment:'#parent'*/
	animate:true,
	aspectRatio:true,
	ghost:true,
	minHeight:150,
	minWidth:150,
	maxHeight:300,
	maxWidth:300
});
//resizeable wedget option end

//resizale event start

$('#resizeBox').resizable({
	start:function(event, ui){
		$("#start").html(resizeEvent(event,ui));
	},
	resize:function(event, ui){
		$("#resize").html(resizeEvent(event,ui));
	},
	stop:function(event, ui){
		$("#stop").html(resizeEvent(event,ui));
	}
});

function resizeEvent(event,ui){
	var element = "Height :" +ui.size.height+"<br/>";
		element += "Width :" +ui.size.width;
		return element;
}
//resizale event end


//selectable widget/event start
	$("#item").selectable({
		stop:function(){
			var output = "";
			$(".ui-selected").each(function(){
				output += $(this).text()+"</br>";
			});
			$("#showMenu").html(output);
		}
	});
//selectable widget/event end

//sortable widget start
$("#sortableitem").sortable({
	placeholder:'selected-item',
	axis:'y',
	opacity:0.7,
	items:'li[data-vlaue = "framework"]'
});

//sortable widget end

//multiple sortable widget start
$("#framework,#cms").sortable({
	/*connectWith:"#framework,#cms"*/
	connectWith:'ul[data-value = "connect"]'

});
//multiple sortable widget end

//color animation start
var colorAnimate = false;
$('#showAnimate').click(function(){
	var div = $("#divCotent");
	if(colorAnimate == true){
		div.animate({
			"background-color" : "#999",
			"color" : "#444",
			"border-color" : "red",
			"border-width" : "2",
			"font-size" : "15"

		});

	}else{
		div.animate({
			"background-color" : "green",
			"color" : "red",
			"border-color" : "brown",
			"border-width" : "5",
			"font-size" : "25",
			"border-radius":"5"

		});
	}
	colorAnimate = !colorAnimate;
});
//color animation end

//color class trasition start
var trasition = false;
$('#showtransition').click(function(){
	if(trasition){
		$('#transitionID').addClass('transitionDiv',2000,'swing');
	}else{
		$('#transitionID').removeClass('transitionDiv',2000,'swing');
	}

	trasition = !trasition;
});
//color class trasition end

//date picker start
	$("#date").datepicker({
		//appendText:'dd//mm//yyyy',
		showOn:'both',
		buttonText:'show Date',
		dateFormat:'yy/mm/dd',
		//numberOfMonths:2,
		changeMonth:true,
		changeYear:true

	});
//date picker end

//tab create start
	$("#tab").tabs();
//tab create end

//Auto complete widget start
	var string = ["HTML","CSS","JavaScript","JAVA","Jquery","Laravel","AngularJS","Python","PHP"];
	$("#textName").autocomplete({
		source:string,
		autoFocus:true,
		minLength:2,
		delay:500
	});
//Auto complete widget end

//effect widget start

$('#effectDivTwo').click(function() {
  $('#effectDivTwo').effect({
	effect:"explode",
	easing:"easeInExpo",
	pices:5,
	duration:4000
});
});
/*"shake", {
  		time:20,
		distance:200
  },3000,function(){
  	$(this).css({
  		"background-color":"red"
  	});
  }
*/

//effect widget end

//accordian start
$("#doAccordian").accordion();
//accordian end

//spinner widget start
	$("#mySpinner").spinner({
		step:2,
		min:0,
		max:10
	});
//spinner widget end
});

