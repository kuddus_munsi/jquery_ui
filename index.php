<?php include 'inc/header.php';?>
    <div class="maincontent">


		<ol style="margin-left: 40px;">
			<li><a href="formValidation.php">Form Validation</a></li>
			<li><a href="dialougeBox.php">Dialouge Box</a></li>
			<li><a href="tollTip.php">Toll Tip</a></li>
			<li><a href="menuDesing.php">Menu Design</a></li>
			<li><a href="sliderIncrement.php">Slider Increment</a></li>
			<li><a href="rangeSlider.php">Range Slider</a></li>
			<li><a href="imgDimentionSlider.php">Image Dimention Slider Control</a></li>
			<li><a href="dragableWidget.php">Dragable Widget Option</a></li>
			<li><a href="dragableWidgetPriority.php">Dragable Widget Priority</a></li>
			<li><a href="dragableAndDroppable.php">Dragable And Droppable Widget</a></li>
			<li><a href="reSizeableWidget.php">Resizeable Widget</a></li>
			<li><a href="reSizeableEvent.php">Resizeable Event</a></li>
			<li><a href="selectableWidget.php">Selectable Widget</a></li>
			<li><a href="sortableWidget.php">Sortable Widget</a></li>
			<li><a href="multipleSortableWidget.php">Multiple Sortable Widget</a></li>
			<li><a href="setColorAnimation.php">Color Animation by toggoling</a></li>
			<li><a href="classTransitionAnimation.php">Class Transition Animation</a></li>
			<li><a href="datePicker.php">Date Picker</a></li>
			<li><a href="tabCreate.php">Tab Create</a></li>
			<li><a href="autoComleteWidget.php">Auto Complete Widget</a></li>
			<li><a href="effectWidget.php">Effect Widget</a></li>
			<li><a href="accordian.php">Accordian Widget</a></li>
			<li><a href="spinner.php">Spinner Widget</a></li>
		</ol>

     </div>
<?php include 'inc/footer.php';?>
